﻿namespace Adapter_DP
{
    class EnemyRobotAdapter : IEnemyAttacker
    {
        private readonly EnemyRobot _theRobot;

        public EnemyRobotAdapter(EnemyRobot theRobot)
        {
            _theRobot = theRobot;
        }

        public void FireWeapon()
        {
            _theRobot.SmashWithHands();
        }

        public void DriveForward()
        {
            _theRobot.WalkForward();
        }

        public void AssignDriver(string driverName)
        {
            _theRobot.ReactToHuman(driverName);
        }
    }
}

