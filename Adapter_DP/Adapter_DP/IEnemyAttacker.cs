﻿namespace Adapter_DP
{
    public interface IEnemyAttacker
    {
        void FireWeapon();
        void DriveForward();
        void AssignDriver(string driverName);
    }
}
