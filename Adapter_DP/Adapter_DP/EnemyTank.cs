﻿using System;

namespace Adapter_DP
{
    public class EnemyTank : IEnemyAttacker
    {
        private readonly Random _generator = new Random();

        public void AssignDriver(string driverName)
        {
            Console.WriteLine($"{driverName} is driving the Tank");
        }

        public void DriveForward()
        {
           int movement = _generator.Next(5) + 1;

            Console.WriteLine($"Enemy Tank moves {movement} steps");
        }

        public void FireWeapon()
        {
            int attackDamage = _generator.Next(10) + 1;

            Console.WriteLine($"Enemy Tank Does {attackDamage} damage"); 
        }
    }
}
