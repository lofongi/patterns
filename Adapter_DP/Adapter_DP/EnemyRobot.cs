﻿using System;

namespace Adapter_DP
{
    public class EnemyRobot
    {
        private readonly Random _generator = new Random();

        public void SmashWithHands()
        {
            int attackDamage = _generator.Next(10) + 1;

            Console.WriteLine($"Enemy Robot Causes {attackDamage} Damage With Its Hands");
        }

        public void WalkForward()
        {
            int movement = _generator.Next(5) + 1;

            Console.WriteLine($"Enemy Robot Walks Forward {movement} steps");
        }

        public void ReactToHuman(string driverName)
        {
            Console.WriteLine($"Enemy Robot Tramps on {driverName}");
        }
    }
}
