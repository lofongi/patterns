﻿using System;

namespace Adapter_DP
{
    class Program
    {
        static void Main()
        {
            EnemyTank tank = new EnemyTank();

            EnemyRobot robot = new EnemyRobot();

            IEnemyAttacker enemy = new EnemyRobotAdapter(robot);

            robot.ReactToHuman("John");
            robot.SmashWithHands();
            robot.WalkForward();

            Console.WriteLine();

            tank.AssignDriver("Bobby");
            tank.DriveForward();
            tank.FireWeapon();

            Console.WriteLine();

            enemy.AssignDriver("Fill");
            enemy.DriveForward();
            enemy.FireWeapon();

            Console.ReadLine();
        }
    }
}
