﻿using System;

namespace Prototype_DP
{
    class Program
    {
        static void Main()
        {
            CloneFactory animalFactory = new CloneFactory();

            Sheep sally = new Sheep();

            Sheep clonedSheep = (Sheep) animalFactory.Clone(sally);

            Console.WriteLine(sally);

            Console.WriteLine(clonedSheep);

            Console.WriteLine("Sally HashCode: " + sally.GetHashCode());

            Console.WriteLine("ClonedSheep HashCode: " + clonedSheep.GetHashCode());

            Console.ReadLine();
        }
    }
}
