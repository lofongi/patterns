﻿using System;

namespace Prototype_DP
{
    public class Sheep : IAnimal
    {
        public Sheep()
        {
            Console.WriteLine("Sheep is Made");
        }

        public object Clone()
        {
            Console.WriteLine("Sheep is Being Made");

            Sheep sheep = null;

            try
            {
                sheep = (Sheep) MemberwiseClone();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            return sheep ?? throw new InvalidOperationException();
        }

        public override string ToString()
        {
            return "My Hero!";
        }
    }
}
