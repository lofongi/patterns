﻿namespace Prototype_DP
{
    public class CloneFactory
    {
        public object Clone(IAnimal animal)
        {
            return animal.Clone();
        }
    }
}
