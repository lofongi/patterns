﻿using System;

namespace Facade_DP
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccountFacade accessingBabk = new BankAccountFacade(456, 55);

            accessingBabk.WithdrawCash(60.00);

            accessingBabk.WithdrawCash(600.00);

            accessingBabk.DepositCash(300.00);

            Console.ReadLine();
        }
    }
}
