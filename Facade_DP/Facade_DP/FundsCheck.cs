﻿using System;

namespace Facade_DP
{
    public class FundsCheck
    {
        private double _cashInAccount = 1000.00;

        public double GetCashInAccount
        {
            get
            {
                return _cashInAccount;
            }
        }

        public void DecreseCashInAccount(double cashWithdrawn)
        {
            _cashInAccount -= cashWithdrawn;
        }

        public void IncreseCashInAccount(double cashDeposited)
        {
            _cashInAccount += cashDeposited;
        }

        public bool HaveEnoughMoney(double cashToWithdrawal)
        {
            if (cashToWithdrawal > GetCashInAccount)
            {
                Console.WriteLine("Not enough cash!");
                Console.WriteLine($"Current ballance is {GetCashInAccount}");

                return false;
            }
            else
            {
                DecreseCashInAccount(cashToWithdrawal);
                Console.WriteLine($"Success! Your ballance: {GetCashInAccount}");
                return true;
            }
        }

        public void MakeDeposite(double cashToDeposit)
        {
            IncreseCashInAccount(cashToDeposit);

            Console.WriteLine($"Deposited {cashToDeposit}, ballance is: {GetCashInAccount}");
        }
    }
}
