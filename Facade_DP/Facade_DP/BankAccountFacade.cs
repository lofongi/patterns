﻿

using System;

namespace Facade_DP
{
    public class BankAccountFacade
    {
        private int _accountNumber;

        private int _securityCode;


        private AccountNumberCheck _accountChecker;

        private SecurityCodeCheck _securityCodeCheck;

        private FundsCheck _fundsChecker;


        private WelcomeToBank _welcomeToBank;

        public BankAccountFacade(int newAccountNum, int newSecCode)
        {
            _accountNumber = newAccountNum;
            _securityCode = newSecCode;

            _welcomeToBank = new WelcomeToBank();
            _accountChecker = new AccountNumberCheck();
            _securityCodeCheck = new SecurityCodeCheck();
            _fundsChecker = new FundsCheck();
        }


        public int GetAccountNumber
        {
            get { return _accountNumber; }
        }

        public int GetSecurityCode
        {
            get { return _securityCode; }
        }

        public void WithdrawCash(double cashToGet)
        {
            if (_accountChecker.AccountActice(GetAccountNumber) &&
                _securityCodeCheck.IsCodeCorrect(GetSecurityCode) &&
                _fundsChecker.HaveEnoughMoney(cashToGet))
            {
                Console.WriteLine("Transaction Complete\n");
            }
            else
            {
                Console.WriteLine("Transaction faided\n'");
            }
        }


        public void DepositCash(double cashToDeposite)
        {
            if (_accountChecker.AccountActice(GetAccountNumber) &&
                _securityCodeCheck.IsCodeCorrect(GetSecurityCode))
            {
                _fundsChecker.MakeDeposite(cashToDeposite);

                Console.WriteLine("Transaction Complete\n");
            }
            else
            {
                Console.WriteLine("Transaction faided\n'");
            }
        }
    }
}
