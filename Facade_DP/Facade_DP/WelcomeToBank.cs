﻿using System;

namespace Facade_DP
{
    public class WelcomeToBank
    {
        public WelcomeToBank()
        {
            Console.WriteLine("Welcome to Bank!");
            Console.WriteLine("We are happy to give your money if we can find it");
        }
    }
}
