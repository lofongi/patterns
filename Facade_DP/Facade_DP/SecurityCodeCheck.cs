﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade_DP
{
    public class SecurityCodeCheck
    {
        private readonly int _securityCode = 55;

        public int GetSecurityCode
        {
            get { return _securityCode; }
        }

        public bool IsCodeCorrect(int securityCodeToCheck)
        {
            if (securityCodeToCheck == GetSecurityCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
