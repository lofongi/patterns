﻿namespace Facade_DP
{
    public class AccountNumberCheck
    {
        private readonly int _accountNumber = 456;

        public int GetAccountNumber
        {
            get { return _accountNumber; }
        }

        public bool AccountActice(int acctNumToCheck)
        {
            if (acctNumToCheck == GetAccountNumber)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
